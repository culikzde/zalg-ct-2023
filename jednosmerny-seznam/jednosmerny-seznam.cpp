#include <iostream>
using namespace std;

struct Item {
    string name;
    int r, g, b;
    Item* next;
    Item (string name0 = "", int r0 = 0, int g0 = 0, int b0 = 0)
        :
        name (name0), r (r0), g (g0), b (b0), next (nullptr)
    { }
};

Item* first = nullptr;

void print()
{
    Item* p = first;
    while (p != nullptr)
    {
        cout << p->name << " " << p->r << " " << p->g << " " << p->b << endl;
        p = p->next;
    }
}

void insertFirst (Item* p)
{
    p->next = first;
    first = p;
}

void insertLast (Item* p)
{
    if (first == nullptr)
    {
        first = p;
    }
    else
    {
        Item* t = first;
        while (t->next != nullptr)
        {
            t = t->next;
        }
        t->next = p;
    }

    p->next = nullptr;
}

Item* find(string name0)
{
    Item* p = first;

    while (p != nullptr && p->name != name0) { p = p->next; }

    // p == nullptr || p->name == name0

    return p;
}

void clean()
{
    Item* p = first;
    while (p != nullptr)
    {
        Item* t = p->next;
        delete p;
        p = t;
    }
    first = nullptr
}

int main()
{
    insertFirst ( new Item ("cervena", 255, 0, 0) );
    insertFirst (new Item ("zelena", 0, 255, 0));
    insertLast (new Item ("modra", 0, 0, 255));

    print ();
    clean ();


    cout << "O.K." << endl;

    /*
    first = new Item;
    first->name = "red";
    first->r = 255;
    first->g = 0;
    first->b = 0;
    first->next = nullptr;

    Item* t = new Item;
    t->name = "green";
    t->r = 0;
    t->g = 255;
    t->b = 0;
    t->next = nullptr;
    first->next = t;
    */


}

#if 0
void swap(int & a, int & b)
{
    cout << "a= " << a << ", b = " << b << endl;
    int t = a;
    a = b;
    b = t;
    cout << "a= " << a << ", b = " << b << endl;
}

void uswap (int *a, int *b)
{
    cout << "a= " << *a << ", b = " << *b << endl;
    int t = *a;
    *a = *b;
    *b = t;
    cout << "a= " << *a << ", b = " << *b << endl;
}

int x = 10;
int y = 20;

int main()
{
    cout << "x= " << x << ", y = " << y << endl;
    // uswap(&x, &y);
    swap(x, y);
    cout << "x= " << x << ", y = " << y << endl;
}
#endif


#include <iostream>
#include <cassert>
// #include <assert.h>
using namespace std;

struct Item
{
    int key = 0;
    Item* left = nullptr;
    Item* right = nullptr;
};

Item* find (Item* top, int value)
{
    Item* p = top;
    while (p != nullptr && p->key != value)
    {
        if (value < p->key)
            p = p->left;
        else
            p = p->right;
    }
    // p == nullptr || p->key == value;
    return p;
}

Item* search(Item* p, int value)
{
    if (p != nullptr && p->key != value)
    {
        if (value < p->key)
            p = search(p->left, value);
        else
            p = search(p->right, value);
    }
    return p;
}

void insert (Item * & p, int value)
{
    if (p == nullptr)
    {
        p = new Item;
        p->key = value;
        p->left = nullptr;
        p->right = nullptr;
    }
    else if (p->key == value)
    {
        // jiz je ve stromu
    }
    else if (value < p->key)
    {
        insert (p->left, value);
    }
    else
    {
        insert (p->right, value);
    }
}

Item* unlink (Item * & u)
{
    if (u->right != nullptr)
    {
        return unlink (u->right);
    }
    else
    {
        Item* v = u;
        u = u->left;
        return v;
    }

}

void remove(Item*& p, int value)
{
    if (p == nullptr)
    {
        // hodnota neni ve stromu, neni co mazat
    }
    else if (value < p->key)
    {
       remove (p->left, value);
    }
    else if (value > p->key)
    {
       remove (p->right, value);
    }
    else // if (p->key == value)
    {
        // basel jsem prevek k vymazani
        Item* t = p;
        if (p->left == nullptr)
        {
            // vlevo nic, vpravo podstrom nebo nic
            p = p->right;
        }
        else if (p->right == nullptr)
        {
            // vpravo nic, vlevo podstrom
            p = p->left;
        }
        else
        {
            // dva podstromy
            Item * v = unlink (p->left); // v .. nahradnik
            v->left = p->left;
            v->right = p->right;
            p = v; // zmenim "ukotveni"
        }

        delete t;
    }
}

void print(Item* p, int level = 1) {
    if (p!=nullptr) {
        print(p->left, level+1);

        for (int i = 2; i <=level; i++)
        {
            cout << "---- ";
        }
        cout<<p->key<<endl;

        print(p->right, level+1);
    }
}

Item* root = nullptr;

int main()
{
    insert(root, 4);
    insert(root, 2);
    insert(root, 8);
    insert(root, 7);
    insert(root, 9);

    for (int i = 20; i <= 30; i++) insert(root, i);


    print(root);

    cout << "*********" << endl;

    while (root != nullptr)
    {
        if (root->left != nullptr)
            remove (root, root->left->key);
        else if(root->right != nullptr)
            remove (root, root->right->key);
        else
            remove (root, root->key);
    }

    print(root);

    Item* t = find (root, 7);

    if (t == nullptr)
        cout << "Nenasel jsem" << endl;
    else
        cout << "Nasel jsem" << endl;

    cout << "O.K." << endl;
}
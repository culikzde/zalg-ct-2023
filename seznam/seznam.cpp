#include <iostream>
#include <cassert>
// #include <assert.h>
using namespace std;

class Item;

class List 
{
public:
    Item* first = nullptr;
    Item* last = nullptr;

private:
    void insert(Item* before, Item* fresh, Item* after);
    
public:

    List () { }
    ~ List ();

    Item* getFirst() { return first; }
    Item* getLast() { return last; }

    void insertFirst (Item* fresh);
    void insertLast(Item* fresh);
    
    Item * find (string name0);
    void print ();

    friend class Item;
};

class Item {
public:
    string name;
    int r, g, b;

// private:
    List* link;
    Item* prev;
    Item* next;

public:
    Item(string name0 = "", int r0 = 0, int g0 = 0, int b0 = 0) :
        name(name0), r(r0), g(g0), b(b0), link (nullptr), prev(nullptr), next(nullptr)
    { }

    Item* getPrev() { return prev; }
    Item* getNext() { return next; }

    void insertBefore (Item* fresh);
    void insertAfter (Item* fresh);
    void remove();

};

List :: ~ List()
{
    Item* p = first;
    while (p != nullptr)
    {
        Item * nxt = p->next;
        // p->remove();
        delete p;
        p = nxt;
    }
    // first = nullptr;
    // last = nullptr;
}

void List::insert(Item* before, Item* fresh, Item* after)
{
    assert (fresh != nullptr);
    assert (fresh->link == nullptr);

    fresh->link = this;

    fresh->prev = before;
    fresh->next = after;

    if (before!=nullptr)
    {
       before->next = fresh;
    }
    else
    {
        first = fresh;
    }

    if (after!=nullptr)
    {
        after->prev = fresh;
    }
    else
    {
        last = fresh;
    }
}

void Item::remove()
{
    assert(link!=nullptr);
    Item* before = prev;
    Item* after = next;
    
    if (before != nullptr)
        before->next = after;
    else 
        link->first = after;

    if (after != nullptr)
        after->prev = before;
    else
        link->last = before;

    prev = nullptr;
    next = nullptr;
    link = nullptr;
}

void List::insertFirst(Item* fresh)
{
    insert (nullptr, fresh, first);
}

void List::insertLast(Item* fresh)
{
    this->insert (this->last, fresh, nullptr);
}

void Item::insertBefore(Item* fresh)
{
    link->insert (prev, fresh, this);
}

void Item::insertAfter(Item* fresh)
{
    this->link->insert (this, fresh, this->next);
}

Item* List::find(string name0)
{
    Item* p = first;
    while (p != nullptr && p->name != name0)
    {
        p = p->next;
    }
    // p == nullptr || p->name == name0;
    return p;
}

void List::print ()
{
    cout << "zacatek seznamu" << endl;
    Item* p = first;
    while (p != nullptr)
    {
        cout << p->name << " " << p->r << " " << p->g << " " << p->b << endl;
        p = p->next;
    }
    cout << "konec seznamu" << endl;
    cout << endl;
}

List a;

int main()
{
    a.print();

    a.insertFirst(new Item("cervena", 255, 0, 0));
    a.insertLast (new Item("zelena", 0, 255, 0));
    
    a.getFirst() -> insertAfter(new Item("modra", 0, 0, 255));
    a.getLast() -> insertBefore (new Item("zluta", 255, 255, 0));

    a.print();

    List b;

    b.print();

    while (a.getFirst() != nullptr)
    {
        Item* p = a.getFirst();
        p->remove();
        b.insertFirst(p);
    }
    cout << "----" << endl;
    a.print();
    b.print();

    cout << "O.K." << endl;
}
    
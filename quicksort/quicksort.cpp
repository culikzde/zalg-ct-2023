#include <iostream>
#include <assert.h>
#include <time.h>
using namespace std;

const int N = 1000;

int a[N]; //= { 4, 1, 8, 3, 9, 11, 4, 5, 7, 2 };

void swap (int& x, int& y)
{
    int t = x;
    x = y;
    y = t;
}

void quickSort(int i, int j)
{
    int pivot = a[(i+j)/2];
    int dolni = i;
    int horni = j;
    while (dolni <= horni) {
        while (a[dolni] < pivot) {
            ++dolni;
        }
        while (a[horni] > pivot) {
            --horni;
        }
        if (dolni <= horni) {
            swap(a[horni], a[dolni]);
            ++dolni;
            --horni;
        }
    }
    if (i < horni) {
        quickSort(i, horni);
    }
    if (j > dolni) {
        quickSort(dolni, j);
    }
}

void init()
{
    srand(time(nullptr));
    for (int i = 0; i < N; i++)
        a[i] = rand() % 10000 + 10000 * (rand() % 10000);
}

void check() {
    for (int i = 0; i < N - 1; i++) {
        if (a[i] > a[i + 1]) cout << "CHYBA" << endl;
    }
}

void print()
{
    for (int i = 0; i < N; i++)
    {
        cout << a[i] << endl;
    }
}

int main()
{
    init();
    quickSort(0, N-1);
    print();
    check();
    cout << "O.K." << endl;
}

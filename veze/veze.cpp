#include <iostream>
#include <assert.h>
using namespace std;

const int N = 5;

struct Vez{
    int v;
    int a[N];
};

void Vynuluj(Vez &X)
{
    X.v = 0;
    for (int i = 0; i < N; ++i) {
        X.a[i] = 0;
    }
}

void Napln(Vez& X)
{
    X.v = N;
    for (int i = 0; i < N; ++i) {
        X.a[i] = N - i;
    }
}

void krok (Vez& X, Vez& Y) 
{
    assert (X.v > 0);
    int d = X.a [X.v-1]; // velikost prenaseneho disku
    X.v -= 1;

    assert (Y.v < N);
    if (Y.v > 0) 
    {
        assert (Y.a[Y.v-1] > d); // soucasny vrchni disk a novy disk
    }
    Y.v ++;
    Y.a [Y.v-1] = d;
}

void Print();

void hraj (Vez& X, Vez& Y, Vez& Z, int k)
{
   if(k>1) hraj (X, Z, Y, k-1); // X --> Y, k-1 disku
   krok(X, Z); // X-->Z, 1 disk
   Print ();
   if(k>1) hraj (Y, X, Z, k-1); // X-->Z, k-1 disku
}


Vez A;
Vez B;
Vez C;

void Print (Vez& X) 
{
    for (int i = 0; i < X.v; i++) {
        cout << X.a[i];
        if (i < X.v-1) { cout << ", "; } else { cout << "  "; }
    }
    for (int i = X.v; i < N; i++) {
        cout << "   ";
    }
}

void Print() {
    Print(A); cout << " : ";
    Print(B); cout << " : ";
    Print(C); cout << endl;
}

int main()
{
    Napln(A);
    Vynuluj(B);
    Vynuluj(C);

    Print();
    hraj(A, B, C, N);
        

    /*krok(A, B);
    Print();

    krok(A, C);
    Print();

    krok(B, C);
    Print();*/

    cout << "O.K." << endl;
}


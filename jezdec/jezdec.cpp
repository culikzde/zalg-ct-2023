#include <iostream>
using namespace std;

const int N = 8;
int a[N+1][N+1];
int tx[N+1][N+1];
int ty[N+1][N+1];

void play(int x, int y,int cnt=0, int x0=0, int y0=0) {
	if (x>=1 && x<=8 && y>=1 && y<=8 ) {
		if (a[x][y]==-1 || a[x][y]>cnt) {

			a[x][y] = cnt;
			tx[x][y] = x0;
			ty[x][y] = y0;

			play(x+1, y+2, cnt+1, x, y);
			play(x-1, y-2, cnt+1, x, y);
			play(x-1, y+2, cnt+1, x, y);
			play(x+1, y-2, cnt+1, x, y);

			play(x+2, y+1, cnt+1, x, y);
			play(x-2, y-1, cnt+1, x, y);
			play(x-2, y+1, cnt+1, x, y);
			play(x+2, y-1, cnt+1, x, y);
		}
	}
}

void cesta(int x, int y) {

	if (x>0 && y>0) {
		cesta(tx[x][y], ty[x][y]);
		cout << x << ", " << y << "\n";
	}
}

int main()
{
	for (int i = 0; i < N+1; i++)
	{
		for (int k = 0; k < N+1; k++)
		{
			a[i][k] = -1;
			tx[i][k] = 0;
			ty[i][k] = 0;
		}
	}

	int x1 = 2, x2 = 7, y1 = 2, y2 = 7;
	for (int i = x1; i <= x2; i++)
	{
		a[i][y1] = -2;
		a[i][y2] = -2;
	}
		for (int k = y1; k<= x2; k++)
		{
			a[x1][k] = -2;
			a[x2][k] = -2;
		}

	play(3, 3);

	for (int i = 1; i<=N; i++) {
		for (int k = 1; k<=N; k++) {
			if (a[i][k]==-2) {
				cout <<"x ";
			}
			else if (a[i][k]==-1) {
				cout <<". ";
			}
			else {
				cout << a[i][k]<< " ";
			}
		} 
		cout << endl;
	}

	cesta(8, 8);

    cout << "O.K." << endl;
}


#include <iostream>
#include <fstream>
#include <time.h>

#include <cassert>
// #include <assert.h>

#undef UNICODE
#include <windows.h>

using namespace std;

const int Max = 4;

struct Item
{
    int cnt; // pocet klicu 
    int key [Max+1]; // key[0], ... key[cnt-1], a jeden navic
    Item* ref [Max+2]; // ref [0], ... ref [cnt], a jeden navic
    Item ();
};

Item::Item()
{
    cnt = 0;
    for (int i = 0; i < Max + 1; i++) key[i] = -1;
    for (int i = 0; i < Max + 2; i++) ref[i] = nullptr;
}

/* ---------------------------------------------------------------------- */

void enter0 (Item* p, int val)
{
    int i = 0; 
    while (i<p->cnt && p->key[i]<val) { i++; }
    
    if (p->ref[0] == nullptr)
    {
        // jsem na nejnizsim patre, zatridime hodnotu

        if (i < p->cnt && p->key[i] == val)
        {
            // hodnota je jiz ve stromu
        }
        else
        {
            for (int j = p->cnt-1; j>=i; j--) { p->key[j+1] = p->key[j]; }
            p->key[i] = val;

            for (int j = p->cnt; j>=i+1; j--) { p->ref[j+1] = p->ref[j]; }
            p->ref[i+1] = nullptr;

            p->cnt++;
        }
    }
    else
    {
        Item* t = p->ref[i]; // blok, do ktereho mam vlozit hodnotu
        enter0 (t, val);
        if (t->cnt > Max)
        {
            // t je preplneno, rozdelit
            int a = Max / 2; // ponecham v t ( v levem bloku )
            int d = t->key[a]; // klic, ktery postoupi vyse
            int b = t->cnt - a - 1; // pujde do praveho bloku

            // vlozit hodnotu d do horniho bloku na pozici i
            for (int j = p->cnt-1; j>=i; j--) { p->key[j+1] = p->key[j]; }
            p->key[i] = d; // hodnota, ktera postoupila vyse
            for (int j = p->cnt; j>=i+1; j--) { p->ref[j+1] = p->ref[j]; }
            p->ref[i+1] = nullptr; // zmenim
            p->cnt++;

            // novy  pravy blok
            Item* n = new Item;
            n->cnt = b;
            for (int j = 0; j < b; j++) { n->key[j] = t->key[a+1+j]; } // prestehuji b klicu
            for (int j = 0; j < b+1; j++) { n->ref[j] = t->ref[a+1+j]; } // prestehuji b+1 ukazatelu
            p->ref[i+1] = n; // odkaz na pravy blok

            // zkratit levy blok
            for (int j = a; j < t->cnt; j++) { t->key[j] = -1; } // neni treba
            for (int j = a+1; j < t->cnt+1; j++) { t->ref[j] = nullptr; } 
            t->cnt = a; // nastavit pocet prvku v levem bloku
        }
    }
}

/* ---------------------------------------------------------------------- */

void enter (Item*& p, int val)
{
    if (p == nullptr)
    {
        p = new Item;
        p->cnt = 1;
        p->key[0] = val;
        p->ref[0] = nullptr;
        p->ref[1] = nullptr;
    }
    else
    {
        /* strom ji� existuje */
        enter0 (p, val);
        Item* t = p; 
        if (t->cnt > Max)
        {
            // t je preplneno, rozdelit
            int a = Max / 2; // ponecham v t ( v levem bloku )
            int d = t->key[a]; // klic, ktery postoupi vyse
            int b = t->cnt - a - 1; // pujde do praveho bloku

            // novy  pravy blok
            Item* n = new Item;
            n->cnt = b;
            for (int j = 0; j < b; j++) { n->key[j] = t->key[a+1+j]; } // prestehuji b klicu
            for (int j = 0; j < b+1; j++) { n->ref[j] = t->ref[a+1+j]; } // prestehuji b+1 ukazatelu

            // zkratit levy blok
            for (int j = a; j < t->cnt; j++) { t->key[j] = -1; } // neni treba
            for (int j = a+1; j < t->cnt+1; j++) { t->ref[j] = nullptr; }
            t->cnt = a; // nastavit pocet prvku v levem bloku

            // novy korenovy blok
            p = new Item; // prepisu odkaz na koren stromu
            p->cnt = 1;
            p->key[0] = d;
            p->ref[0] = t; // t ... schovany odkaz na puvodni korenovy blok
            p->ref[1] = n;
        }
    }
}

/* ---------------------------------------------------------------------- */

void display(ofstream& f, Item* p, bool comma = false, int level = 0)
{
    if (p != nullptr)
    {
        bool list = (p->ref[0] == nullptr);
        for (int i = 1; i <= level; i++) f << "   ";
        f << "[";

        // klice v jednoduchych uvozovkach
        f << "'";
        for (int i = 0; i < p->cnt; i++)
        {
            f << p->key[i];
            if (i < p->cnt-1) f << ", ";
        }
        f << "'";

        if (!list)
        {
            f << ","; // carka pred nasledujicimi hodnotam
            f << endl;
            for (int i = 0; i < p->cnt + 1; i++)
            {
                display(f, p->ref[i], i < p->cnt, level+1);
            }
        }

        if (!list)
            for (int i = 1; i <= level; i++) f << "   ";
        f << "]";
        if (comma) f << ",";
        f << endl;
    }
}

void displayData (string fileName, Item* p)
{

    ofstream f (fileName);

    f << "<html>" << endl;
    f << "<head>" << endl;
    f << "<title>B-Tree with mxGraph</title>" << endl;
    f << "<script type=\"text/javascript\">" << endl;
    f << "    mxBasePath = 'http://jgraph.github.io/mxgraph/javascript/src';" << endl;
    f << "</script>" << endl;
    f << "<script type=\"text/javascript\" src=\"http://jgraph.github.io/mxgraph/javascript/src/js/mxClient.js\"></script>" << endl;
    f << "<script type=\"text/javascript\">" << endl;

    f << "    var data =" << endl;
    display(f, p);
    f << ";" << endl;

    f << "function display (graph, parent, above, items)" << endl;
    f << "{" << endl;
    f << "    var name;" << endl;
    f << "    if (Array.isArray (items))" << endl;
    f << "       name = items [0];" << endl;
    f << "    else" << endl;
    f << "       name = items;" << endl;
    f << endl;
    f << "    var v1 = graph.insertVertex (parent, null, name, 0, 0, 80, 30);" << endl;
    f << "    if (above != null)" << endl;
    f << "       var e1 = graph.insertEdge(parent, null, '', above, v1);" << endl;

    f << "    if (Array.isArray (items))" << endl;
    f << "    {" << endl;
    f << "      var i;" << endl;
    f << "      for (i = 1; i < items.length; i++)" << endl;
    f << "      {" << endl;
    f << "         var item = items [i];" << endl;
    f << "         display (graph, parent, v1, items [i]);" << endl;
    f << "      }" << endl;
    f << "    }" << endl;
    f << "    return v1;" << endl;
    f << "}" << endl;
    f << "function main (container)" << endl;
    f << "{" << endl;
    f << "    var graph = new mxGraph(container);" << endl;
    f << "    var parent = graph.getDefaultParent();" << endl;
    f << "    graph.getModel().beginUpdate();" << endl;
    f << "    var v1 = display (graph, parent, null, data);" << endl;
    f << "    var layout = new mxHierarchicalLayout (graph, mxConstants.DIRECTION_NORTH);" << endl;
    f << "    layout.execute (graph.getDefaultParent(), v1);" << endl;
    f << "    graph.getModel().endUpdate();" << endl;
    f << "};" << endl;
    f << "</script>" << endl;
    f << "</head>" << endl;
    f << "<body onload=\"main(document.getElementById('graphContainer'))\">" << endl;
    f << "<div id=\"graphContainer\">" << endl;
    f << "</div>" << endl;
    f << "</body>" << endl;
    f << "</html>" << endl;

    f.close();

    char path[MAX_PATH];
    GetCurrentDirectory(MAX_PATH, path);
    fileName = string("file:///") + path + "/" + fileName;

    string cmd = "\"C:/Program Files/Mozilla Firefox/firefox\" " + fileName;
    // string cmd = "\"C:/Program Files/Mozilla Firefox/firefox\" file:///C:/Users/culikzde/source/repos/zalg-ct-2023/avl/" + fileName;

    system(cmd.c_str());
}

/* ---------------------------------------------------------------------- */

Item * root = nullptr;

int main()
{
    /*
    enter (root, 30);
    enter (root, 20);
    enter (root, 10);
    enter (root, 40);
    enter (root, 50);

    for (int i = 1; i <= 40; i++)
       enter (root, i);
    */


    srand(time(nullptr));
    for (int i = 1; i < 40; i++)
        enter (root, rand() % 100);

    displayData ("output.html", root);

    cout << "O.K." << endl;
}